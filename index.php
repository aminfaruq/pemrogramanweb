<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <title>Hello, world!</title>
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container">
      <img src="img/Logo.svg" alt="">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Service</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Courses</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact Us</a>
          </li>
          <button class="btn btn-primary">Log In</button>
        </ul>
      </div>
    </div>
    </nav>
    <!-- Akhir navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid d-flex align-items-center">
        <div class="container ">
          <div class="row">
            <div class="col-lg-8">
                <h1 class="display-4">Get Access to Unlimited Educational Resources. Everywhere, Everytime!</h1>
                <p class="lead">premium access to more than 10,000 resources ranging from courses, events e.t.c.</p>
                <button class="btn btn-primary">Get Access</button>  
            </div>
          </div>
          
        </div>
      </div>
    <!-- Akhir Jumbotron -->
    
    <!-- Services -->
    <section class="services d-flex align-items-center justify-content-center mt-0" id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mb-5">
              <h4>Services</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <img src="img/service1.svg" alt="">
            <h6>Unlimited Access</h6>
            <p>One subscription unlimited access</p>
          </div>
          <div class="col-lg-4">
            <img src="img/service2.svg" alt="">
            <h6>Expert Teachers</h6>
            <p>Learn from industry experts who are passionate about teaching</p>
          </div>
          <div class="col-lg-4">
            <img src="img/service3.svg" alt="">
            <h6>Learn Anywhere</h6>
            <p>Switch between your computer, tablet, or mobile device.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir services -->

    <!-- Stories -->
    <section class="stories d-flex align-items-center" id="stories">
      <div class="container">
        <div class="row">
         <div class="col-lg-4">
          <h1>Success Stories From Our Students WorldWide!</h1>
          <p>Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone looking to pursue a career in digital marketing, Accounting, Web development, Programming. Multimedia and CAD design.</p>
          <button class="btn btn-primary">Discover</button>
        </div>
         <div class="col-lg-8 text-center">
          <img src="img/stories-img.png" alt="" width="80%">
         </div>
        </div>
      </div>
    </section>
    <!-- Akhir stories -->

    <!-- Courses -->
    <section class="courses d-flex align-items-center" id="courses">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h4>Courses</h4>
          </div>
        </div>
        <div class="row pb-4">
          <div class="col-lg-12 text-center">
            <a href="">All</a>
            <a href="">Design</a>
            <a href="">Web Development</a>
            <a href="">Digital</a>
            <a href="">Photography</a>
            <a href="">Motion Graphics</a>
            <a href="">Digital Marketing</a>
          </div>
        </div>

        <div class="row">

        <?php
        include "koneksi.php";
        $data = mysqli_query($koneksi, "select*from courses");
        while($d = mysqli_fetch_array($data)) {
        ?>
        <div class="col-lg-3">
            <div class="card">
              <div class="card-top text-center pt-2">
                <?php echo $d['pricing'] ?>
              </div>
                <img src="img/<?php echo $d['img']?>" class="card-img-top" alt="...">
                <div class="card-body">
                  <p><?php echo $d['description'] ?></p>
                </div>
              </div>
        </div>
        

        <?php
        }
        ?>
        </div>
            
            <div class="row justify-content-center mt-5">
                <button class="btn btn-primary">Discover</button>
            </div>

      </div>
    </section>
    <!-- Akhir Courses -->

    <!-- Achievement -->

    <section class="achievement d-flex align-items-center" id="achievement">
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <img src="img/Achievement1.svg" alt="">
            <h1>5,679</h1>
            <p>Registered Students</p>
          </div>
          <div class="col-lg-3">
            <img src="img/Achievement2.svg" alt="">
            <h1>2,679</h1>
            <p>Student has been helped to achieve their dreams</p>
          </div>
          <div class="col-lg-3">
            <img src="img/Achievement3.svg" alt="">
            <h1>10,000</h1>
            <p>More than 10,000 people visits our site monthly</p>
          </div>
          <div class="col-lg-3">
            <img src="img/Achievement4.svg" alt="">
            <h1>#10</h1>
            <p>Ranked among the top 10 growing online learning startups in West Africa</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Akhir Achievement -->

    <!-- Testimonials -->
    <section class="testimonials d-flex align-items-center" id="testimonials">
      <div class="container-fluid">
      <div class="row text-center">
        <div class="col-lg-12">
            <h1>What Students Say</h1>
        </div>
      </div>
      <div class="row text-center mb-3 justify-content-center">
        <div class="col-lg-8">
            <p>Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist,.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                 <img src="img/testi.svg" alt="">
                  <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                  <div class="row">
                    <div class="col-lg-3">
                      <img src="img/User-Pic.svg" alt="">
                    </div>
                    <div class="col-lg-8 pt-2 ml-2">
                      <h5>Arthur Broklyn</h5>
                      <p>Categories: 3d Modelling</p>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                 <img src="img/testi.svg" alt="">
                  <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                  <div class="row">
                    <div class="col-lg-3">
                      <img src="img/User-Pic.svg" alt="">
                    </div>
                    <div class="col-lg-8 pt-2 ml-2">
                      <h5>Arthur Broklyn</h5>
                      <p>Categories: 3d Modelling</p>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                 <img src="img/testi.svg" alt="">
                  <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                  <div class="row">
                    <div class="col-lg-3">
                      <img src="img/User-Pic.svg" alt="">
                    </div>
                    <div class="col-lg-8 pt-2 ml-2">
                      <h5>Arthur Broklyn</h5>
                      <p>Categories: 3d Modelling</p>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                 <img src="img/testi.svg" alt="">
                  <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
                  <div class="row">
                    <div class="col-lg-3">
                      <img src="img/User-Pic.svg" alt="">
                    </div>
                    <div class="col-lg-8 pt-2 ml-2">
                      <h5>Arthur Broklyn</h5>
                      <p>Categories: 3d Modelling</p>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </div>
    </section>
    <!-- Akhir testimonials -->
    
    <!-- Newsletter -->
    <section class="newsletter d-flex align-items-center" id="newsletter">
      <div class="container">
        <img src="img/Rectangle7.svg" alt="" class="imgs">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h4 class="display-h4">Subscribe to Our Newsletter</h4>
          <p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
          <button class="btn btn-primary">Start free trial</button>
        </div>
      </div>
      </div>
    </section>
    <!-- Akhir Newsletter -->

    <!-- Contact -->
    <section class="contact d-flex align-items-center" id="contact">
      <div class="row justify-content-center">
        <div class="col-lg-3">
          <img src="img/Logo.svg" alt="" class="mb-3">
          <p>Semaj Africa is an online education platform that delivers video courses, programs and resources.</p>
          <img src="img/sosmed.svg" alt="" class="mt-5">
        </div>
        <div class="col-lg-2">
          <h6>Quicklinks</h6>
          <ul>
            <li>Home</li>
            <li>Courses</li>
            <li>About Us</li>
            <li>Contact Us</li>
          </ul>
        </div>
        <div class="col-lg-3">
          <h6>Contact Us</h6>
          <ul>
            <li>(+55) 254. 254. 254</li>
            <li>Info@lsemajafrica.com</li>
            <li>Helios Tower 75 Tam Trinh Hoang</li>
            <li>Mai - Ha Noi - Viet Nam</li>
          </ul>
        </div>
        <div class="col-lg-3">
          <h6>Terms and Conditions Faq</h6>
        </div>
      </div>
    </section>
    <!-- Akhir Contact -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>